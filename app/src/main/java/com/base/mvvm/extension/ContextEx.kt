package com.base.mvvm.extension

import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.provider.Settings
import com.virtuesense.api_response.AppInformationModel

fun Context.getDeviceId(): String {
    return Settings.Secure.getString(
            this.contentResolver,
            Settings.Secure.ANDROID_ID
    )
}

fun Context.getApplicationInfo(context: Context): AppInformationModel {
    val appInformation = AppInformationModel()
    try {
        val packageInfo = packageManager.getPackageInfo(packageName, 0)
        appInformation.version_name = (packageInfo.versionName)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            appInformation.version_code = (packageInfo.longVersionCode) as Int
        } else {
            appInformation.version_code = (packageInfo.versionCode)
        }
        appInformation.package_name = (packageInfo.packageName)
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
    }
    return appInformation
}