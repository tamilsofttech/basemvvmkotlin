package com.base.mvvm.extension

import java.text.SimpleDateFormat
import java.util.*

fun String.convertToInteger(value: String?): Int {
    var result = 0
    try {
        if (value != null && value.trim { it <= ' ' } != "") {
            result = Integer.parseInt(value.trim { it <= ' ' })
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return result
}

fun String.convertToDouble(value: String?): Double {
    var result = 0.0
    try {
        if (value != null && value.trim { it <= ' ' } != "") {
            result = java.lang.Double.parseDouble(value.trim { it <= ' ' })
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }
    return result
}

fun String.convertToLong(value: String?): Long {
    var result: Long = 0
    try {
        if (value != null && value.trim { it <= ' ' } != "") {
            result = java.lang.Long.parseLong(value.trim { it <= ' ' })
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return result
}

fun String.convertToFloat(value: String?): Float {
    var result = 0f
    try {
        if (value != null && value.trim { it <= ' ' } != "") {
            result = java.lang.Float.parseFloat(value.trim { it <= ' ' })
        }
    } catch (e: Exception) {
        e.printStackTrace()
    }

    return result
}


