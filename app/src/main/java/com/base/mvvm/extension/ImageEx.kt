package com.base.mvvm.extension

import android.widget.ImageView
import com.squareup.picasso.Picasso
import java.io.File


fun ImageView.showImage(file: File?) {
    if (file != null && file.isFile)
        Picasso.get().load(file).fit().into(this)
}

fun ImageView.showImage(url: String?) {
    if (url != null && url != "")
        Picasso.get().load(url).into(this)
}

fun ImageView.showImage(url: String?, placeholder: Int) {
    if (url != null && url != "")
        Picasso.get().load(url).fit().placeholder(placeholder).into(this)
}

fun ImageView.showImage(file: File?, placeholder: Int) {
    if (file != null && file.isFile)
        Picasso.get().load(file).fit().placeholder(placeholder).into(this)
}