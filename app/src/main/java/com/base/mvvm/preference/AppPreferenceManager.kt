package playground.androidjetpack.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.base.mvvm.MyApplication
import com.base.mvvm.R
import com.securepreferences.SecurePreferences
import java.util.*

class AppPreferenceManager(myApplication: MyApplication) {

    init {
        myApplication.appComponent.inject(this)
        initPreference(myApplication.applicationContext)
    }

    private fun initPreference(context: Context) {
        prefs = SecurePreferences(
                context,
                context.getString(R.string.preference_password),
                context.getString(R.string.preference_name)
        )
    }

    private fun write(key: String, value: String) {
        val editor = prefs!!.edit()
        editor.putString(key, value).apply()

    }

    private fun write(key: String, value: Int) {
        val editor = prefs!!.edit()
        editor.putInt(key, value).apply()
    }

    private fun write(key: String, value: Boolean) {
        val editor = prefs!!.edit()
        editor.putBoolean(key, value).apply()
    }

    private fun getInt(key: String): Int {
        return if (prefs!!.contains(key)) {
            prefs!!.getInt(key, -1)
        } else -1
    }

    private fun getString(key: String): String? {
        return if (prefs!!.contains(key)) {
            prefs!!.getString(key, null)
        } else ""
    }

    fun clearAll() {
        val editor = prefs!!.edit()
        editor.remove(KEY_USER_ID)
        editor.apply()
    }

    private fun generateTempToken(): String {
        return UUID.randomUUID().toString().replace(
                "-".toRegex(),
                ""
        ) + UUID.randomUUID().toString().replace("-".toRegex(), "")
    }

    fun setEncryptionKey() {
        if (!hasEncryptionKey()) {
            write(KEY_DB, generateTempToken())
        }
    }

    fun setAccessToken(token: String) {
        write(KEY_USER_TOKEN, token)
    }

    fun getAccessToken(): String {
        return getString(KEY_USER_TOKEN).toString()
    }

    fun setUserId(userId: String) {
        write(KEY_USER_ID, userId)
    }

    fun getUserId() {
        getString(KEY_USER_ID)
    }

    private fun hasEncryptionKey(): Boolean {
        return !TextUtils.isEmpty(getString(KEY_DB))
    }

    fun getEncryptionKey(): String? {
        return getString(KEY_DB)
    }

    companion object {
        private const val KEY_USER_ID = "user.id"
        private const val KEY_USER_TOKEN = "user.token"
        private const val KEY_DB = "encryptionKeyDB"
        private var prefs: SharedPreferences? = null
    }
}
