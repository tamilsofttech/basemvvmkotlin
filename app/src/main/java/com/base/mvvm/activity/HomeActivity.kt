package com.base.mvvm.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import com.base.mvvm.BaseActivity
import com.base.mvvm.R
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.extension.getDeviceId
import com.base.mvvm.utils.Vars.App_Start
import com.base.mvvm.viewmodel.HomeViewModel
import com.google.firebase.iid.FirebaseInstanceId

class HomeActivity : BaseActivity() {

    lateinit var homeViewModel: HomeViewModel
    lateinit var appComponent: AppComponent

    override fun injectComponent(appComponent: AppComponent) {
        this.appComponent = appComponent
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        homeViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java!!)
        if (appComponent != null)
            appComponent.inject(homeViewModel)

        if (App_Start == "0") {
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                val newToken = instanceIdResult.token
                Log.e("newToken", newToken)
                homeViewModel.getUpdateToken(appPreferenceManager.getAccessToken(), getDeviceId(), newToken);
                App_Start = "1"
            }.addOnCompleteListener { }
        }
    }

}
