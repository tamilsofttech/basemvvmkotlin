package com.base.mvvm.activity

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import com.base.mvvm.BaseActivity
import com.base.mvvm.R
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.viewmodel.RegisterViewModel

class RegisterActivity : BaseActivity() {

    lateinit var viewModel: RegisterViewModel
    lateinit var appComponent: AppComponent

    override fun injectComponent(appComponent: AppComponent) {
        this.appComponent = appComponent
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        viewModel = ViewModelProviders.of(this).get(RegisterViewModel::class.java!!)
        if (appComponent != null)
            appComponent.inject(viewModel)
    }
}
