package com.base.mvvm.activity

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.base.mvvm.BaseActivity
import com.base.mvvm.R
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.viewmodel.ForgetPasswordViewModel
import com.base.mvvm.viewmodel.IndexViewModel

class ForgetPasswordActivity : BaseActivity()  {

    lateinit var viewModel: ForgetPasswordViewModel
    lateinit var appComponent: AppComponent

    override fun injectComponent(appComponent: AppComponent) {
        this.appComponent = appComponent
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)

        viewModel = ViewModelProviders.of(this).get(ForgetPasswordViewModel::class.java!!)
        if (appComponent != null)
            appComponent.inject(viewModel)
    }
}
