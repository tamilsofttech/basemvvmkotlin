package com.base.mvvm.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.base.mvvm.BaseActivity
import com.base.mvvm.R
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.databinding.ActivityLoginBinding
import com.base.mvvm.viewmodel.LoginViewModel
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import java.util.*


class LoginActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {

    lateinit var viewModel: LoginViewModel
    lateinit var appComponent: AppComponent

    private lateinit var callbackManager: CallbackManager
    private lateinit var loginButton: LoginButton
    private var mGoogleApiClient: GoogleApiClient? = null
    private val RC_SIGN_IN = 100
    internal var type = ""
    var strDeviceId = ""
    var strDeviceType = "Android"

    override fun injectComponent(appComponent: AppComponent) {
        this.appComponent = appComponent
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login)
        binding.activityAction = MyClickHandler()

        viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java!!)
        if (appComponent != null)
            appComponent.inject(viewModel)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .addOnConnectionFailedListener(this)
                .build()

        loginButton = findViewById<View>(R.id.login_button) as LoginButton
        loginButton.setReadPermissions(Arrays.asList("email"))
        callbackManager = CallbackManager.Factory.create()

        loginButton.registerCallback(callbackManager,
                object : FacebookCallback<LoginResult> {
                    override fun onSuccess(loginResult: LoginResult) {

                        val request = GraphRequest.newMeRequest(
                                loginResult.accessToken
                        ) { `object`, _ ->
                            try {
                                val name = `object`.getString("name")
                                val strEmail = `object`.getString("email")

                                val strSource = "facebook"

                                if (LoginManager.getInstance() != null) {
                                    LoginManager.getInstance().logOut()
                                }
                                callRetroSocialLogin(name, strEmail, strSource)

                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                        val parameters = Bundle()
                        parameters.putString("fields", "id,name,email")
                        request.parameters = parameters
                        request.executeAsync()
                    }

                    override fun onCancel() {
                        Log.e("Error", "Cancel")
                    }

                    override fun onError(exception: FacebookException) {

                    }
                })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)
        } else if (type == "facebook") {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            val acct = result.signInAccount
            if (acct != null) {
                val name = acct.displayName
                val strEmail = acct.email
                val strSource = "google"

                signOut()
                callRetroSocialLogin(name!!, strEmail!!, strSource)
            } else
                Toast.makeText(this, "Authentication failure", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show()
        }
    }

    private fun signOut() {
        mGoogleApiClient!!.connect()
        mGoogleApiClient!!.registerConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
            override fun onConnected(bundle: Bundle?) {
                if (mGoogleApiClient!!.isConnected()) {
                    Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback { }
                }
            }

            override fun onConnectionSuspended(i: Int) {

            }
        })
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {}

    private fun callRetroSocialLogin(strName: String, strEmail: String, str_source: String) {
//        Custom_Progress.visibility = View.VISIBLE
//
//        webService.getSocialLogin(strName, strEmail, str_source, strDeviceId, strDeviceType)
//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(
//                        { response ->
//                            Custom_Progress.visibility = View.GONE
//                            if (response.status == "false")
//                                util.showAlertWithTitle(this, "Error", response.message!!)
//                            else
//                                updateLogin(response)
//                        },
//                        { error ->
//                            Custom_Progress.visibility = View.GONE
//                            if (Vars.SHOW_ERROR == "true") {
//                                util.showAlertWithTitle(this, "API Error", error.toString()!!)
//                            }
//                            Log.e("Error", error.toString())
//                        }
//                )
    }

    inner class MyClickHandler {

        fun onFacebookClicked() {
            type = "facebook"
            loginButton.performClick()
        }

        fun onGoogleClicked() {
            type = "google"
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
    }
}
