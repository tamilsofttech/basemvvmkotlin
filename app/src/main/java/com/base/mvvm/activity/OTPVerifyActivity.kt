package com.base.mvvm.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.base.mvvm.BaseActivity
import com.base.mvvm.R
import com.base.mvvm.dagger.AppComponent

class OTPVerifyActivity : BaseActivity()  {

    lateinit var appComponent: AppComponent

    override fun injectComponent(appComponent: AppComponent) {
        this.appComponent = appComponent
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otpverify)
    }
}
