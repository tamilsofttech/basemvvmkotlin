package com.base.mvvm

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import com.base.mvvm.activity.HomeActivity
import com.base.mvvm.activity.LoginActivity
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.utils.Vars.staUserInfo
import com.base.mvvm.viewmodel.BaseViewModel
import com.base.mvvm.viewmodel.IndexViewModel


class Index : BaseActivity() {

    lateinit var indexViewModel: IndexViewModel
    lateinit var appComponent: AppComponent

    override fun injectComponent(appComponent: AppComponent) {
        this.appComponent = appComponent
        appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_index)

        indexViewModel = ViewModelProviders.of(this).get(IndexViewModel::class.java!!)

        if (appComponent != null) {
            appComponent.inject(indexViewModel)
        }

        if (appPreferenceManager.getAccessToken() == "") {
            var intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        } else {
            indexViewModel.getUserInfo(appPreferenceManager.getAccessToken()).observe(this, Observer { userInfo ->
                if (userInfo!!.status == "false") {

                } else {
                    staUserInfo = userInfo.data
                    var intent = Intent(this, HomeActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
            })
        }
    }
}

