package com.base.mvvm.utils

import android.annotation.SuppressLint
import com.base.mvvm.apiresponse.Res_UserInfo

@SuppressLint("StaticFieldLeak")
object Vars {

    var App_Start: String = "0";
    lateinit var staUserInfo: Res_UserInfo

    const val DatePattern1 = "dd-MM-yyyy hh:mm a"
    const val DatePattern2 = "EEE, MMM dd yyyy hh:mm a"
    const val DatePattern3 = "dd.MM.yyyy"
    const val DatePattern4 = "dd-MM-yyyy"
    const val DatePattern5 = "yyyy-MM-dd"
    const val DatePattern6 = "yyyy-MM-dd HH:mm:ss"
    const val DatePattern7 = "EEE, MMM dd yyyy"
    const val DatePattern8 = "hh:mm a"
    const val DatePattern9 = "yyyy-MM-dd hh:mm:ss a"

}