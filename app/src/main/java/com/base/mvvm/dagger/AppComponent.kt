package com.base.mvvm.dagger

import com.base.mvvm.Index
import com.base.mvvm.MyApplication
import com.base.mvvm.activity.*
import com.base.mvvm.viewmodel.*
import dagger.Component
import playground.androidjetpack.utils.AppPreferenceManager
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun inject(myApplication: MyApplication)
    fun inject(appPreferenceManager: AppPreferenceManager)

    //Activity Inject
    fun inject(index: Index)

    fun inject(loginActivity: LoginActivity)
    fun inject(registerActivity: RegisterActivity)
    fun inject(forgetPasswordActivity: ForgetPasswordActivity)
    fun inject(otpVerifyActivity: OTPVerifyActivity)
    fun inject(homeActivity: HomeActivity)


    //View Model Inject
    fun inject(indexViewModel: IndexViewModel)

    fun inject(baseViewModel: BaseViewModel)
    fun inject(loginViewModel: LoginViewModel)
    fun inject(registerViewModel: RegisterViewModel)
    fun inject(forgetPasswordViewModel: ForgetPasswordViewModel)
    fun inject(homeViewModel: HomeViewModel)

}