package com.base.mvvm.dagger

import com.base.mvvm.BuildConfig
import com.base.mvvm.MyApplication
import com.base.mvvm.webservice.ApiServiceInterface
import com.base.mvvm.webservice.ApiServiceInterface1
import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.ConnectionSpec
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import playground.androidjetpack.utils.AppPreferenceManager
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class AppModule(myApplication: MyApplication) {
    var myApplication: MyApplication = myApplication

    // Web service
    // Pref
    // Database

    @Provides
    @Singleton
    fun provideGson(): Gson {
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
        gsonBuilder.setLenient()
        gsonBuilder.setPrettyPrinting()
        gsonBuilder.setPrettyPrinting()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .serializeNulls()
        return gsonBuilder.create()
    }

    companion object {
        private var logLevel = if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        val okHttpLogLevel = logLevel
    }

    @Provides
    @Singleton
    internal fun provideOkHttpClient(): OkHttpClient {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = okHttpLogLevel

        return OkHttpClient.Builder()
                .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                .addInterceptor(loggingInterceptor)
                //.addInterceptor(ConnectivityInterceptor(baseApp.applicationContext))
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()
    }


    @Provides
    internal fun provideAppAPI(gson: Gson, okHttpClient: OkHttpClient): ApiServiceInterface {
        var retrofit = Retrofit.Builder()
                .baseUrl("http://shamlatech.net/android_portal/sample/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build()

        return retrofit.create(ApiServiceInterface::class.java)
    }


    @Provides
    internal fun provideAppAPI1(gson: Gson, okHttpClient: OkHttpClient): ApiServiceInterface1 {
        var retrofit = Retrofit.Builder()
                .baseUrl("http://moveonsense.virtuesense.com:8080/rider/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build()

        return retrofit.create(ApiServiceInterface1::class.java)
    }

    @Provides
    @Singleton
    fun provideAppPreferenceManager(): AppPreferenceManager {
        return AppPreferenceManager(myApplication)
    }
}