package com.base.mvvm

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.webservice.ApiServiceInterface
import com.base.mvvm.webservice.ApiServiceInterface1
import playground.androidjetpack.utils.AppPreferenceManager
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity() {

    lateinit var myApplication: MyApplication;

    @Inject
    lateinit var webApiService: ApiServiceInterface

    @Inject
    lateinit var appPreferenceManager: AppPreferenceManager

    @Inject
    lateinit var webApiService1: ApiServiceInterface1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        myApplication = applicationContext as MyApplication
        injectComponent(myApplication.appComponent)
    }


    protected abstract fun injectComponent(appComponent: AppComponent)
}