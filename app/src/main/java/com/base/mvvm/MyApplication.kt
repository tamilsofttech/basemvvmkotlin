package com.base.mvvm

import android.support.multidex.MultiDexApplication
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.dagger.AppModule
import com.base.mvvm.dagger.DaggerAppComponent

class MyApplication : MultiDexApplication() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        initComponent()
    }

    private fun initComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(AppModule(this))
                .build()

        appComponent.inject(this)
    }


}