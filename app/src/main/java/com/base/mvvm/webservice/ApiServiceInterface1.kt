package com.base.mvvm.webservice

import dagger.Provides
import retrofit2.http.GET
import retrofit2.http.Query
import rx.Observable
import java.util.*

interface ApiServiceInterface1 {

    @GET("account/app-setting")
    fun getAppSettingService123(@Query("country") country: String): Observable<Object>


}