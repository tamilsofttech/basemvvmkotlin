package com.base.mvvm.webservice

import com.base.mvvm.apiresponse.Result_Common
import com.base.mvvm.apiresponse.Result_UserInfo
import com.base.mvvm.apiresponse.Result_UserList
import rx.Observable


object ApiServiceCall {

    fun getAccessLogin(strEmail: String, strCCP: String, strPhone: String, strPassword: String, strDeviceId: String, strDeviceType: String, webApiService: ApiServiceInterface): Observable<Result_UserInfo> {
        return webApiService.getAccessLogin(strEmail, strCCP, strPhone,  strPassword, strDeviceId, strDeviceType)
    }

    fun getAccessUserInfo(id: String, webApiService: ApiServiceInterface): Observable<Result_UserInfo> {
        return webApiService.getAccessUserInfo(id)
    }

    fun getAccessUpdateToken(id: String, device_id: String, token: String, webApiService: ApiServiceInterface): Observable<Result_Common> {
        return webApiService.getUpdateToken(id, device_id, token)
    }


    fun getAccessUpdateLogout(id: String, device_id: String, webApiService: ApiServiceInterface): Observable<Result_Common> {
        return webApiService.getAccessUpdateLogout(id, device_id)
    }

    fun getAccessUserList(country: String, webApiService: ApiServiceInterface): Observable<Result_UserList> {
        return webApiService.getAccessUserList(country)
    }

    fun getAccessAppSetting(country: String, webApiService: ApiServiceInterface): Observable<Object> {
        return webApiService.getAppSettingService(country)
    }

    fun getAccessAppSetting123(country: String, webApiService: ApiServiceInterface1): Observable<Object> {
        return webApiService.getAppSettingService123(country)
    }
}