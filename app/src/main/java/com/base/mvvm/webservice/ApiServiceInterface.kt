package com.base.mvvm.webservice

import com.base.mvvm.apiresponse.Result_Common
import com.base.mvvm.apiresponse.Result_UserInfo
import com.base.mvvm.apiresponse.Result_UserList
import retrofit2.http.*
import rx.Observable

interface ApiServiceInterface {

    @GET("sample1.json")
    fun getAppSettingService(@Query("country") country: String): Observable<Object>

    @POST("account/login")
    @FormUrlEncoded
    fun getAccessLogin(@Field("email") email: String, @Field("ccp") ccp: String,
                       @Field("phone") phone: String, @Field("password") password: String,
                       @Field("device_type") device_type: String, @Field("device_id") device_id: String): Observable<Result_UserInfo>


    @GET("sample1.json")
    fun getAccessUserList(@Query("country") country: String): Observable<Result_UserList>


    @GET("sample1.json")
    fun getAccessUserInfo(@Query("id") id: String): Observable<Result_UserInfo>

    @GET("sample1.json")
    fun getUpdateToken(@Query("id") id: String,
                       @Query("device_id") device_id: String,
                       @Query("token") token: String): Observable<Result_Common>

    @GET("sample1.json")
    fun getAccessUpdateLogout(@Query("id") id: String,
                              @Query("device_id") device_id: String): Observable<Result_Common>

}