package com.base.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.base.mvvm.dagger.AppComponent
import com.base.mvvm.webservice.ApiServiceInterface
import com.base.mvvm.webservice.ApiServiceInterface1
import com.trello.rxlifecycle.components.RxFragment
import playground.androidjetpack.utils.AppPreferenceManager
import javax.inject.Inject

abstract class BaseFragment : RxFragment() {
    lateinit var myApplication: MyApplication;

    @Inject
    lateinit var webApiService: ApiServiceInterface

    @Inject
    lateinit var appPreferenceManager: AppPreferenceManager

    @Inject
    lateinit var webApiService1: ApiServiceInterface1

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        myApplication = activity as MyApplication
        injectComponent(myApplication!!.appComponent)

        return super.onCreateView(inflater, container, savedInstanceState)
    }

    protected abstract fun injectComponent(appComponent: AppComponent)
}