package com.base.mvvm.viewmodel

import android.app.Activity
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.base.mvvm.apiresponse.Result_UserInfo
import com.base.mvvm.webservice.ApiServiceCall
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class LoginViewModel : BaseViewModel() {

    fun getLogin(strEmail: String, strCCP: String, strPhone: String, strPassword: String, strDeviceId: String, strDeviceType: String): MutableLiveData<Result_UserInfo> {
        userInfo = MutableLiveData()
        ApiServiceCall.getAccessLogin(strEmail, strCCP, strPhone, strPassword, strDeviceId, strDeviceType, webApiService)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            userInfo.postValue(response)
                            Log.e("Response", response.toString())
                        },
                        { error ->
                            Log.e("Error", error.toString())
                        }
                )
        return userInfo;
    }


}