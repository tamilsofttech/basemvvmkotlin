package com.base.mvvm.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.base.mvvm.apiresponse.UserList
import com.base.mvvm.webservice.ApiServiceCall
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers


class RegisterViewModel : BaseViewModel() {


    var userList = MutableLiveData<ArrayList<UserList>>()

    init {

    }

    fun getLogin(subCategoryId: String): MutableLiveData<ArrayList<UserList>> {
        userList = MutableLiveData()
        ApiServiceCall.getAccessUserList("India", webApiService)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            userList.postValue(response.data)
                            Log.e("Response", response.toString())
                        },
                        { error ->
                            Log.e("Error", error.toString())
                        }
                )
        return userList;
    }
}