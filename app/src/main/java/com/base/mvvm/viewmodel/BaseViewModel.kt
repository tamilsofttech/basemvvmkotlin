package com.base.mvvm.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.base.mvvm.apiresponse.Result_UserInfo
import com.base.mvvm.webservice.ApiServiceCall
import com.base.mvvm.webservice.ApiServiceInterface
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

open class BaseViewModel : ViewModel() {

    @Inject
    lateinit var webApiService: ApiServiceInterface

    var userInfo = MutableLiveData<Result_UserInfo>()


    fun getUserInfo(id: String): MutableLiveData<Result_UserInfo> {
        userInfo = MutableLiveData()
        ApiServiceCall.getAccessUserInfo(id, webApiService)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            userInfo.postValue(response)
                            Log.e("Response", response.toString())
                        },
                        { error ->
                            Log.e("Error", error.toString())
                        }
                )
        return userInfo;
    }

    fun getUpdateToken(id: String, device_id: String, token: String) {
        ApiServiceCall.getAccessUpdateToken(id, device_id, token, webApiService)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            Log.e("Response", response.toString())
                        },
                        { error ->
                            Log.e("Error", error.toString())
                        }
                )
    }

    fun getUpdateLogout(id: String, device_id: String) {
        ApiServiceCall.getAccessUpdateLogout(id, device_id, webApiService)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        { response ->
                            Log.e("Response", response.toString())
                        },
                        { error ->
                            Log.e("Error", error.toString())
                        }
                )
    }


}