package com.virtuesense.api_response

import java.io.Serializable

open class AppInformationModel : Serializable {
    open
    var version_name: String? = null
    var version_code: Int? = 0
    var package_name: String? = null
}
