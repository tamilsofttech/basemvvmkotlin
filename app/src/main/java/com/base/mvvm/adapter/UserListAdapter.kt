package com.base.mvvm.adapter

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.base.mvvm.R
import com.base.mvvm.databinding.ListUserListBinding
import com.base.mvvm.apiresponse.UserList


class UserListAdapter(val users: ArrayList<UserList>) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): UserListAdapter.ViewHolder {
        val binding: ListUserListBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.list_user_list, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return users.size
    }

    override fun onBindViewHolder(holder: UserListAdapter.ViewHolder, position: Int) {
        val binding = holder.binding
        binding.item = users.get(position)
    }

    class ViewHolder(val binding: ListUserListBinding) : RecyclerView.ViewHolder(binding.root)

}
