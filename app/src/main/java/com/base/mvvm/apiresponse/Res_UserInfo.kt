package com.base.mvvm.apiresponse

import android.databinding.BaseObservable
import android.databinding.Bindable

class Res_UserInfo : BaseObservable() {
    @Bindable
    var id: String = ""

    @Bindable
    var name: String = ""

    @Bindable
    var image: String = ""

    @Bindable
    var email: String = ""

    @Bindable
    var ccp: String = ""

    @Bindable
    var phone: String = ""

    @Bindable
    var gender: String = ""
}