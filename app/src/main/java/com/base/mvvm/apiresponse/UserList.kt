package com.base.mvvm.apiresponse

import android.databinding.BaseObservable
import android.databinding.Bindable

class UserList : BaseObservable() {
    @Bindable
    var name: String = ""

    @Bindable
    var degree: String = ""
}